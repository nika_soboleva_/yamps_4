package matrixes;

public class MatrixErrorCode {
    public static final String NEGATIVE_DIMENSION = "Matrix dimension was negative %d";
    public static final String ZERO_DIMENSION = "Matrix dimension is 0";
    public static final String INCORRECT_INDEXES = "Incorrect indexes: dimension = %d, row = %d, column = %d";
    public static final String CANNOT_SWAP_STRINGS = "Strings swap can't be done";
}