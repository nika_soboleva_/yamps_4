public enum SquareTrinomialMaxRootExtractorErrorCode {
    NO_REAL_ROOTS("no real roots"),
    NULL_TRINOMIAL("must not be null");
    
    
    private final String errorString;
    
    
    SquareTrinomialMaxRootExtractorErrorCode(String errorString) {
        this.errorString = errorString;
    }
    
    
    public String getErrorString() {
        return errorString;
    }
}
