package functions;


public enum FunctionsErrorCode {
    ARGUMENT_OUT_OF_DOMAIN("The argument is out of the function’s domain"),
    INCORRECT_EPS("An accuracy value must be positive and must not be less than the threshold value"),
    LEFT_GT_RIGHT("The left bound is bigger than the right one"),
    NULL_DENOMINATOR("The denominator can't be 0");
    
    
    private String errorString;
    
    
    FunctionsErrorCode(String errorString) {
        this.errorString = errorString;
    }
    
    
    public String getErrorString() {
        return errorString;
    }
}
