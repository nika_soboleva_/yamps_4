package model;


import org.junit.jupiter.api.Test;

import static error.IllegalArgumentMessage.NULL_DEPARTMENT;
import static org.junit.jupiter.api.Assertions.*;


public class TestStudent {
    @Test
    void testStudent() {
        Student student1 = new Student("Александра", "Олеговна", "Воробьёва", 0, "матфак");
        Student student2 = new Student("Alex", "Black", 21, "russlit");
        
        assertAll(
                () -> assertEquals("Александра", student1.getFirstName()),
                () -> assertEquals("Олеговна", student1.getPatronymicName()),
                () -> assertEquals("Воробьёва", student1.getSecondName()),
                () -> assertEquals(0, student1.getAge()),
                () -> assertEquals("матфак", student1.getDepartment()),
                () -> assertEquals("Alex", student2.getFirstName()),
                () -> assertNull(student2.getPatronymicName()),
                () -> assertEquals("Black", student2.getSecondName()),
                () -> assertEquals(21, student2.getAge()),
                () -> assertEquals("russlit", student2.getDepartment())
        );
    }
    
    
    @Test
    void testSetDepartmentException() {
        try {
            Student student1 = new Student("Елена", "Сергеевна", "Петрова", 17, null);
            fail("student1");
        } catch (IllegalArgumentException e) {
            assertEquals(NULL_DEPARTMENT, e.getMessage());
        }
        
        try {
            Student student2 = new Student("Michel", "Smith", 32, "");
            fail("student2");
        } catch (IllegalArgumentException e) {
            assertEquals(NULL_DEPARTMENT, e.getMessage());
        }
    }
    
    
    @Test
    void testEquals() {
        Student student1 = new Student("Xenia", "Olsson", 21, "Architecture");
        Student student2 = new Student("Xenia", "Olsson", 21, "Architecture");
        Student student3 = new Student("Linn", "Olsson", 21, "Architecture");
        Student student4 = new Student("Xenia", "Jönsson", 21, "Architecture");
        Student student5 = new Student("Xenia", "Olsson", 41, "Architecture");
        Student student6 = new Student("Xenia", "Olsson", 21, "Mathematics");
        
        assertAll(
                () -> assertEquals(student1, student1),
                () -> assertEquals(student1, student2),
                () -> assertEquals(student2, student1),
                () -> assertNotEquals(student1, ""),
                () -> assertNotEquals(student1, student3),
                () -> assertNotEquals(student1, student4),
                () -> assertNotEquals(student1, student5),
                () -> assertNotEquals(student1, student6),
                () -> assertNotEquals(student4, student6)
        );
    }
}
